from tkinter import *
from random import shuffle
from time import sleep

WIDTH = 700
HEIGHT = 500
RED = 'red'
BLUE = 'blue'
BLACK = 'black'
WHITE = 'white'
GREEN = 'green'

class Game:
    '''
    Inicia o Jogo    
    '''
    def __init__(self, master=None):
        self.master = master
        self.master.title('Game')
        self.master.geometry('{}x{}+10+10'.format(WIDTH,HEIGHT))
        #img = PhotoImage(file = 'pleiades.png')
        
        self.canvas = Canvas(self.master, width=WIDTH, height = HEIGHT, highlightthickness = 0, bg = 'black')
        self.canvas.pack()
        self.master.update()
        
    def _canvas(self):
        return self.canvas

class Ball:
    '''
    Define o objeto ball
    '''
    def __init__(self, canvas, color, pa):
        self.pa = pa
        self.canvas = canvas
        self.item = self.canvas.create_oval(10,10,40,40, fill=color, tags = 'ball')
         
        self.canvas.move(self.item, 150, 200)

        array = [-3, -2, -1, 0, 1, 2 , 3]
        shuffle(array)
        
        self.x = array[0]
        self.y = -2
        
        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width = self.canvas.winfo_width()

        self.hit_bottom = False

    def hit(self, pos):
        pos_pa = self.canvas.coords(self.pa.item)

        if pos[2] >= pos_pa[0] and pos[0] <= pos_pa[2]:
            if pos[3] >= pos_pa[1] and pos[3] <= pos_pa[3]:
                print('PA--> ', pos_pa)
                print('Ball--> ', pos)
                return True
        return False
        
    def auto_move(self):
        self.canvas.move('ball', self.x, self.y)
        pos = self.canvas.coords(self.item)
        
        if pos[1] <= 0:
            self.y = 2
            
        if pos[3] >= self.canvas_height:
            self.y = -2
            self.hit_bottom = True
            t = self.canvas.create_text(0,0, text = 'GAME OVER', font = ('verdana','10'), fill=RED)
            self.canvas.move(t, (WIDTH/2), (HEIGHT/2)-50)
            
        if pos[0] <= 0:
            self.x = 2
            
        if pos[2] >= self.canvas_width:
            self.x = -2
            
        if self.hit(pos):
            self.y = -2

        

class Barra:
    #
    #define a barra
    #
    def __init__(self, canvas):
        self.canvas = canvas
        self.item = self.canvas.create_rectangle(0, 0, 150, 15, fill = WHITE, tags = 'barra')
        self.canvas.move('barra', (WIDTH/2)-75, HEIGHT-100)
        self.x = 0
        self.canvas.update_idletasks()
        
        self.canvas.bind_all("<KeyPress-Left>", self.move_left)
        self.canvas.bind_all("<KeyPress-Right>", self.move_right)
        
        self.canvas_width = self.canvas.winfo_width()

    def move(self):
        self.canvas.move('barra', self.x, 0)
        pos = self.canvas.coords(self.item)

        if pos[0] <= 0:
            self.x = 0
        if pos[2] >= self.canvas_width:
            self.x = 0
        
    
    def move_right(self, evt):
        self.x = 2

    def move_left(self, evt):
        self.x = -2

    
def main():
    win = Tk()
    win.resizable(0, 0)
    #win.wm_attributes('-topmost', 1)
    
    game = Game(win)
    canvas = game._canvas()
    
    barra = Barra(canvas)
    redBall= Ball(canvas, WHITE, barra)
    
    while 1:
        if redBall.hit_bottom == False:
            redBall.auto_move()
            barra.move()
        
        win.update_idletasks()
        win.update()
        sleep(0.01)

if __name__=='__main__':
    main()
    
    

